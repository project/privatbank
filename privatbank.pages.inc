<?php

define('PRIVATBANK_DEBUG_ERRORS', true);

function _privatbank_translit($str, $encIn, $encOut){ // transliteration function

    $cyr=array(
    "Щ",  "Ш", "Ч", "Ц","Ю", "Я", "Ж", "А","Б","В","Г","ґ","Д","Е","Ё","З","И","Й","К","Л","М","Н","О","П","Р","С","Т","У","Ф","Х", "Ь","Ы","Ъ","Э","Є","І","Ї",
    "щ",  "ш", "ч", "ц","ю", "я", "ж", "а","б","в","г","Ґ","д","е","ё","з","и","й","к","л","м","н","о","п","р","с","т","у","ф","х", "ь","ы","ъ","э","є","і","ї");

    $lat=array(
    "Shh","Sh","Ch","C","Ju","Ja","Zh","A","B","V","G","G","D","Je","Jo","Z","I","J","K","L","M","N","O","P","R","S","T","U","F","Kh","'","Y","`","E","Je","I","Ji",
    "shh","sh","ch","c","ju","ja","zh","a","b","v","g","g","d","je","jo","z","i","j","k","l","m","n","o","p","r","s","t","u","f","kh","'","y","`","e","je","i","ji"
    );
    
      $str = iconv($encIn, "utf-8", $str);
      for($i=0; $i<count($cyr); $i++){
        $c_cyr = $cyr[$i];
        $c_lat = $lat[$i];
        $str = str_replace($c_cyr, $c_lat, $str);
      }
      $str = preg_replace("/([qwrtpsdfghklzxcvbnmQWRTPSDFGHKLZXCVBNM]+)[jJ]e/", "\${1}e", $str);
      $str = preg_replace("/([qwrtpsdfghklzxcvbnmQWRTPSDFGHKLZXCVBNM]+)[jJ]/", "\${1}'", $str);
      $str = preg_replace("/([eyuioaEYUIOA]+)[Kk]h/", "\${1}h", $str);
      $str = preg_replace("/^kh/", "h", $str);
      $str = preg_replace("/^Kh/", "H", $str);
      
      return iconv("utf-8", $encOut, $str);

}

function privatbank_prefillform($form_state){
   
	$form=array(); $currencies_ar=array();

	$currency_settings = unserialize(variable_get('privatbank_currencies', serialize(_privatbank_GetDefCurSetts())));
	foreach($currency_settings as $key=>$value){
		if($value['enabled']==1) $currencies_ar[$key]=$key;
	}

	
	$form['currency'] = array(
		'#type' => 'radios',
		'#title' => t("Currency"),
		'#options' => $currencies_ar,
		'#default_value' => 'USD',
		//'#description' => t("Select payment currency."),
		'#required' => TRUE
	);
 

	$form['amount'] = array(
		'#type' => 'textfield', 
		'#title' => t('Amount'), 
		'#default_value' => '', 
		'#size' => 10, 
		'#maxlength' => 12, 
		'#required' => TRUE
	);

	$form['memo'] = array(
		'#type' => 'textarea', 
		'#title' => t('Memo'), 
		'#default_value' => t('Payment to !sitename', array('!sitename' => variable_get('site_name', 'Drupal'))), 
		'#description' => t("Payment description."),
		'#required' => TRUE
	);


	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Cerate payment'),
	);

	return $form;

}

function privatbank_prefillform_submit(&$form, $form_state){
	
	$payment=_privatbank_createpayment(array(
		'amount'=>$form_state['values']['amount'],
		'currency'=>$form_state['values']['currency'],
		'memo'=>$form_state['values']['memo'],
	));


	if(is_array($payment) && $payment['pid']>0){
		drupal_set_message(t("Please confirm payment details"));
		drupal_goto('privatbank/payment/'.$payment['pid']);
	}

}

function privatbank_merchantform($form_state, $payment){

	global $base_url;

	if(!is_array($payment) && is_integer($payment)){ // fetch payment info from DB
		$payment=privatbank_pid_load($payment);
	}

	if(!is_array($payment) || !($payment['pid']>0)){

		$form['error'] = array(
		  '#type' => 'item',
		  '#title' => t('Error'),
		  '#value' => t('Order you are going to pay for does not exist'),
		);

	}else{

		$form['#action'] = variable_get('privatbank_payment_url', PRIVATBANK_MERCHANT_URL);
		
		$currency_settings = unserialize(variable_get('privatbank_currencies', serialize(_privatbank_GetDefCurSetts())));
		$presc = $currency_settings["{$payment['currency']}"]['presc'];

		// interface data:

		$form['payment_id'] = array(
		  '#type' => 'item',
		  '#title' => t('Order #'),
		  '#value' => $payment['pid'],
		);

		$form['amount'] = array(
		  '#type' => 'item',
		  '#title' => t('Amount'),
		  '#value' => round($payment['amount'], $presc).' '.$payment['currency'],
		);

		$form['memo'] = array(
		  '#type' => 'item',
		  '#title' => t('Memo'),
		  '#value' => _privatbank_translit($payment['memo'], "utf-8", "cp1251")
		);

		// merchant data...

		$form['merchant'] = array(
			'#type' => 'hidden',
			'#value' => $payment['payee_account'],
		);

		$form['order'] = array(
			'#type' => 'hidden',
			'#value' => $payment['pid'],
		);
	 
		$form['details'] = array(
			'#type' => 'hidden',
			'#value' => _privatbank_translit($payment['memo'], "utf-8", "cp1251"),
		);

		$form['amt'] = array(
			'#type' => 'hidden',
			'#value' => round($payment['amount'], $presc),
		);

		$form['ccy'] = array(
			'#type' => 'hidden',
			'#value' => $payment['currency'],
		);

		/*$form['PAYEE_NAME'] = array(
			'#type' => 'hidden',
			'#value' => variable_get('privatbank_payee_name',  variable_get('site_name', 'Drupal')),
		);*/

		$form['server_url'] = array(
			'#type' => 'hidden',
			'#value' => $base_url.'/'.drupal_get_path_alias('privatbank/status'),
		);

		$form['return_url'] = array(
			'#type' => 'hidden',
			'#value' => $base_url.'/privatbank/success',
		);

		$form['pay_way'] = array(
			'#type' => 'hidden',
			'#value' => 'privat24',
		);
		$form['ext_details'] = array(
			'#type' => 'hidden',
			'#value' => '',
		);

		$form['submit'] = array(
			'#type' => 'submit',
			'#value' => t('Pay now'),
		);

	}

	return $form;

}

function _privatbank_parse_query($query){

	$items=explode("&", $query);

	$ar=array();

	foreach($items as $it){
		$key=""; $value="";
		list($key, $value)=explode("=", $it, 2);
		$ar[$key]=$value;
	}

	return $ar;

}

function privatbank_success(){

	$created=time();

	$hash = sha1(md5($_POST['payment'].variable_get('privatbank_secretkey', '')));

	$pb_payment=_privatbank_parse_query($_POST['payment']);

	if(preg_match("/^[0-9]{1,10}$/", $pb_payment['order']) && $hash==$_POST['signature']){ // proccessing payment if only hash is valid

	   $payment=privatbank_pid_load($pb_payment['order']);
	   if(!is_array($payment)){
	      if(PRIVATBANK_DEBUG_ERRORS){
		     $f=fopen("pblog.txt", "ab");
			 fwrite($f, date("m/d/Y H:i:s", $created).' return_url error: payment not found'."\n");
			 fwrite($f,var_export($_POST, true)."\n");
			 fclose($f);
		  }
		  return theme('privatbank_fail');
	   }
		
	   // check if payment already enrolled
	   if($payment['enrolled']>0){
	      if(PRIVATBANK_DEBUG_ERRORS){
		     $f=fopen("pblog.txt", "ab");
			 fwrite($f,date("m/d/Y H:i:s", $created).' return_url error: payment already enrolled'."\n");
			 fwrite($f,var_export($_POST, true)."\n");
			 fclose($f);
		  }
		  return theme('privatbank_success');
	   }

	   $currency_settings = unserialize(variable_get('privatbank_currencies', serialize(_privatbank_GetDefCurSetts())));
	   $presc = $currency_settings["{$payment['currency']}"]['presc'];


	   if($pb_payment['amt']==round($payment['amount'], $presc) && $pb_payment['merchant']==$payment['payee_account'] && $pb_payment['ccy']==$payment['currency']){

		  // enroll payment
		  $result = db_query('UPDATE {privatbank} SET batch=\'%s\', payer_account=\'%s\', enrolled=%d WHERE pid=%d', $pb_payment['ref'], $pb_payment['sender_phone'], $created, $payment['pid']);
					
		  // fire hook
		  $payment['batch']=$pb_payment['ref'];
		  $payment['payer_account']=$pb_payment['sender_phone'];
		  $payment['enrolled']=$created;
		  module_invoke_all('privatbank', 'enrolled', $payment['pid'], $payment);

		  if(PRIVATBANK_DEBUG_ERRORS){
		     $f=fopen("pblog.txt", "ab");
			 fwrite($f,date("m/d/Y H:i:s", $created).' return_url payment OK!'."\n");
			 fwrite($f,var_export($_POST, true)."\n");
			 fclose($f);
		  }

		  return theme('privatbank_success');

	   }else{ // you can also save invalid payments for debug purposes

	      if(PRIVATBANK_DEBUG_ERRORS){
		     $f=fopen("pblog.txt", "ab");
			 fwrite($f,date("m/d/Y H:i:s", $created).' return_url error: fake data'."\n");
			 fwrite($f,var_export($_POST, true)."\n");
			 fclose($f);
		  }
		  return theme('privatbank_fail');

	   }


	}else{ // you can also save invalid payments for debug purposes

	   // uncomment code below if you want to log requests with bad hash
	   if(PRIVATBANK_DEBUG_ERRORS){
	      $f=fopen("pblog.txt", "ab");
		  fwrite($f,date("m/d/Y H:i:s", $created).' return_url error: bad hash or payment_id'."\n");
		  fwrite($f,var_export($_POST, true)."\n");
		  fclose($f);
	   }
	   return theme('privatbank_fail');

	}

}

function privatbank_fail(){
	return theme('privatbank_fail');
}

function privatbank_status(){

	drupal_set_header('Content-type: text/html; charset=iso-8859-1');

	// check url
	$url=trim($_SERVER['REQUEST_URI'], '/');
	$alias=drupal_get_path_alias('privatbank/status');
	if($url!=$alias) die();

	$created=time();

	$hash = sha1(md5($_POST['payment'].variable_get('privatbank_secretkey', '')));

	$pb_payment=_privatbank_parse_query($_POST['payment']);

	if(preg_match("/^[0-9]{1,10}$/", $pb_payment['order']) && $hash==$_POST['signature']){ // proccessing payment if only hash is valid

	   $payment=privatbank_pid_load($pb_payment['order']);
	   if(!is_array($payment)){
	      if(PRIVATBANK_DEBUG_ERRORS){
		     $f=fopen("pblog.txt", "ab");
			 fwrite($f,date("m/d/Y H:i:s", $created).' server_url error: payment not found'."\n");
			 fwrite($f,var_export($_POST, true)."\n");
			 fclose($f);
		  }
		  die();
	   }

	   // check if payment already enrolled
	   if($payment['enrolled']>0){
	      if(PRIVATBANK_DEBUG_ERRORS){
		     $f=fopen("pblog.txt", "ab");
			 fwrite($f,date("m/d/Y H:i:s", $created).' server_url error: payment already enrolled'."\n");
			 fwrite($f,var_export($_POST, true)."\n");
			 fclose($f);
		  }
		  die();
	   }

	   $currency_settings = unserialize(variable_get('privatbank_currencies', serialize(_privatbank_GetDefCurSetts())));
	   $presc = $currency_settings["{$payment['currency']}"]['presc'];


	   if($pb_payment['amt']==round($payment['amount'], $presc) && $pb_payment['merchant']==$payment['payee_account'] && $pb_payment['ccy']==$payment['currency']){

		  // enroll payment
		  $result = db_query('UPDATE {privatbank} SET batch=\'%s\', payer_account=\'%s\', enrolled=%d WHERE pid=%d', $pb_payment['ref'], $pb_payment['sender_phone'], $created, $payment['pid']);
					
		  // fire hook
		  $payment['batch']=$pb_payment['ref'];
		  $payment['payer_account']=$pb_payment['sender_phone'];
		  $payment['enrolled']=$created;
		  module_invoke_all('privatbank', 'enrolled', $payment['pid'], $payment);

		  if(PRIVATBANK_DEBUG_ERRORS){
		     $f=fopen("pblog.txt", "ab");
			 fwrite($f,date("m/d/Y H:i:s", $created).' server_url payment OK!'."\n");
			 fwrite($f,var_export($_POST, true)."\n");
			 fclose($f);
		  }

		  die();

	   }else{ // you can also save invalid payments for debug purposes

	      if(PRIVATBANK_DEBUG_ERRORS){
		     $f=fopen("pblog.txt", "ab");
			 fwrite($f,date("m/d/Y H:i:s", $created).' server_url error: fake data'."\n");
			 fwrite($f,var_export($_POST, true)."\n");
			 fclose($f);
		  }
		  die();

	   }


	}else{ // you can also save invalid payments for debug purposes

	   // uncomment code below if you want to log requests with bad hash
	   if(PRIVATBANK_DEBUG_ERRORS){
	      $f=fopen("pblog.txt", "ab");
		  fwrite($f,date("m/d/Y H:i:s", $created).' server_url error: bad hash or payment_id'."\n");
		  fwrite($f,var_export($_POST, true)."\n");
		  fclose($f);
	   }
	   die();

	}

}