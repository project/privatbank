<?php

function privatbank_payments_pre(){

      if (!empty($_POST['payments']) && isset($_POST['operation']) && ($_POST['operation'] == 'delete')) {
        return drupal_get_form('privatbank_payments_delete_confirm');
      }
      else {
        return drupal_get_form('privatbank_payments');
      }

}

function theme_privatbank_payments($form) {
  // Overview table:
  $header = array(
    theme('table_select_header_cell'),
    array('data' => t('ID'), 'field' => 'pid'),
    array('data' => t('Created'), 'field' => 'created'),
    array('data' => t('User'), 'field' => 'uid'),
    array('data' => t('Amount'), 'field' => 'amount'),
    array('data' => t('Currency'), 'field' => 'currency'),
    array('data' => t('Memo')),
    array('data' => t('Privatbank ref'), 'field' => 'batch'),
    array('data' => t('Merchant ID'), 'field' => 'payee_account'),
    array('data' => t('Payer phone'), 'field' => 'payer_account'),
    array('data' => t('Enrolled'), 'field' => 'enrolled'),
  );

  $output = drupal_render($form['options']);
  if (isset($form['created']) && is_array($form['created'])) {
    foreach (element_children($form['created']) as $key) {
      $rows[] = array(
        drupal_render($form['payments'][$key]),
        drupal_render($form['pid'][$key]),
        drupal_render($form['created'][$key]),
        drupal_render($form['name'][$key]),
        drupal_render($form['amount'][$key]),
        drupal_render($form['currency'][$key]),
        drupal_render($form['memo'][$key]),
        drupal_render($form['batch'][$key]),
        drupal_render($form['payee_account'][$key]),
        drupal_render($form['payer_account'][$key]),
        drupal_render($form['enrolled'][$key]),
      );
    }
  }
  else {
    $rows[] = array(array('data' => t('No payments available.'), 'colspan' => '11'));
  }

  $output .= theme('table', $header, $rows);
  if ($form['pager']['#value']) {
    $output .= drupal_render($form['pager']);
  }

  $output .= drupal_render($form);

  return $output;
}

function privatbank_payments(&$form_state){

  $header = array(
    array(),
    array('data' => t('ID')),
    array('data' => t('ID'), 'field' => 'pid', 'sort' => 'desc'),
    array('data' => t('Created'), 'field' => 'created'),
    array('data' => t('User'), 'field' => 'uid'),
    array('data' => t('Amount'), 'field' => 'amount'),
    array('data' => t('Currency'), 'field' => 'currency'),
    array('data' => t('Memo')),
    array('data' => t('Privatbank ref'), 'field' => 'batch'),
    array('data' => t('Merchant ID'), 'field' => 'payee_account'),
    array('data' => t('Payer phone'), 'field' => 'payer_account'),
    array('data' => t('Enrolled'), 'field' => 'enrolled'),
  );

  $currency_settings = unserialize(variable_get('privatbank_currencies', serialize(_privatbank_GetDefCurSetts())));

  $sql = 'SELECT * FROM {privatbank}';
  $sql .= tablesort_sql($header);
  $query_count = 'SELECT COUNT(pid) FROM {privatbank}';
  $result = pager_query($sql, 50, 0, $query_count);

  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Update options'),
    '#prefix' => '<div class="container-inline">',
    '#suffix' => '</div>',
  );
  $form['options']['operation'] = array(
    '#type' => 'select',
    '#options' => array('delete'=>t('Delete selected'), 'enroll'=>t('Enroll selected')),
    '#default_value' => 'delete',
  );
  $form['options']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Apply'),
  );

  $destination = drupal_get_destination();

  $status = array(t('blocked'), t('active'));
  $payments = array();
  while ($payment = db_fetch_object($result)) {
    $payments[$payment->pid] = '';
    $form['pid'][$payment->pid] = array('#value' =>  $payment->pid);
    $form['created'][$payment->pid] = array('#value' => date("m/d/Y H:i", $payment->created));
	$form['name'][$payment->pid] = array('#value' => theme('username', user_load($payment->uid)));
    $form['amount'][$payment->pid] =  array('#value' => round($payment->amount, $currency_settings[$payment->currency]['presc']));
    $form['currency'][$payment->pid] =  array('#value' => $payment->currency);
    $form['memo'][$payment->pid] =  array('#value' => $payment->memo);
    $form['batch'][$payment->pid] =  array('#value' => $payment->batch);
    $form['payee_account'][$payment->pid] =  array('#value' => $payment->payee_account);
    $form['payer_account'][$payment->pid] =  array('#value' => ((!empty($payment->payer_account)) ? $payment->payer_account : '-'));
    $form['enrolled'][$payment->pid] =  array('#value' => (($payment->enrolled>0 && $payment->error=='') ? date("m/d/Y H:i", $payment->enrolled) : (!empty($payment->error) ? '<small><B>Error: </B>'.$payment->error.' ('.date("m/d/Y H:i", $payment->enrolled).')</smal>' : '-')));

  }
  $form['payments'] = array(
    '#type' => 'checkboxes',
    '#options' => $payments
  );
  $form['pager'] = array('#value' => theme('pager', NULL, 50, 0));

  return $form;

}

function privatbank_payments_validate($form, &$form_state) {
  $form_state['values']['payments'] = array_filter($form_state['values']['payments']);
  if (count($form_state['values']['payments']) == 0) {
    form_set_error('', t('No payments selected.'));
  }
}

function privatbank_payments_submit($form, &$form_state) {

  $payments = array_filter($form_state['values']['payments']);
  switch($form_state['values']['operation']){
  case 'enroll':
	  $t=time();
	  foreach ($payments as $pid){
		if(_privatbank_enrollpayment($pid, 'MANUALLY', $t))
			module_invoke_all('privatbank', 'enrolled', $pid);
	  }
	  drupal_set_message(t('The payments have been enrolled.'));
  break;
  }

}

function privatbank_payments_delete_confirm(&$form_state) {

  $edit = $form_state['post'];

  $form['payments'] = array('#tree' => TRUE);

  foreach (array_filter($edit['payments']) as $pid => $value) {

	$form['payments'][$pid] = array('#type' => 'hidden', '#value' => $pid);

  }

  $form['operation'] = array('#type' => 'hidden', '#value' => 'delete');

  return confirm_form($form,
                      t('Are you sure you want to delete selected payments?'),
                      'admin/privatbank/payments', t('This action cannot be undone.'),
                      t('Delete all selected'), t('Cancel'));
}

function privatbank_payments_delete_confirm_submit($form, &$form_state) {

  if ($form_state['values']['confirm']) {
    
	foreach ($form_state['values']['payments'] as $pid => $value) {
		_privatbank_deletepayment($pid);
	}

	drupal_set_message(t('The payments have been deleted.'));
  
  }

  $form_state['redirect'] = 'admin/privatbank/payments';
  
  return;
}




// ---------------------------------------------------------------

function privatbank_settingsform(){

	global $base_url;

	$form=array(); 

	$form['payment_url'] = array(
		'#type' => 'textfield', 
		'#title' => t('Privatbank merchant URL'), 
		'#default_value' => variable_get('privatbank_payment_url', PRIVATBANK_MERCHANT_URL), 
		'#size' => 60, 
		'#maxlength' => 255, 
		'#required' => TRUE
	);

	$form['secret_key'] = array(
		'#type' => 'textfield', 
		'#title' => t('Payment password'), 
		'#default_value' =>  variable_get('privatbank_secretkey', ''), 
		'#size' => 40, 
		'#maxlength' => 100, 
		'#required' => FALSE
	);

	$form['result_url'] = array(
		'#type' => 'textfield', 
		'#title' => t('server_url'), 
		'#default_value' => $base_url.'/'.drupal_get_path_alias('privatbank/status'), 
		'#description' => t("Change default value to increase security"),
		'#size' => 60, 
		'#maxlength' => 255, 
		'#required' => FALSE
	);

	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Save changes'),
	);

	return $form;

}

function privatbank_settingsform_validate($form, &$form_state){
	
	global $base_url;

	if (!empty($form_state['values']['result_url'])){
		if(!preg_match("|^".$base_url."|", $form_state['values']['result_url']))
			form_set_error('result_url', t('You can not change site address, only change path.'));
    }

}

function privatbank_settingsform_submit(&$form, $form_state){

	global $base_url;

	path_set_alias('privatbank/status');

	$form_state['values']['result_url']=trim(str_replace($base_url, '', $form_state['values']['result_url']), '/');

	if($form_state['values']['result_url']!='privatbank/status') {
		path_set_alias('privatbank/status', $form_state['values']['result_url']);
	}

	variable_set('privatbank_payment_url',  $form_state['values']['payment_url']);

	variable_set('privatbank_secretkey',  $form_state['values']['secret_key']);

	drupal_set_message(t("Settings has been saved."));
	drupal_goto('admin/privatbank/settings');

}




// ---------------------------------------------------------------

function privatbank_currencies(){

	$default_currency_settings=_privatbank_GetDefCurSetts();

	$currency_settings = unserialize(variable_get('privatbank_currencies', serialize($default_currency_settings)));


	foreach($currency_settings as $key=>$value){
		
			$row=array();
  
			$row[] = array('data' => $key);
			$row[] = array('data' => ((empty($value['account'])) ? 'n/a' : $value['account']));
			$row[] = array('data' => (($value['enabled']==1) ? 'yes' : 'no'));
			$row[] = array('data' => l(t('edit'), 'admin/privatbank/currencies/edit/'.$key).' '.l(t('delete'), 'admin/privatbank/currencies/delete/'.$key).' '.l(t('view sample'), 'admin/privatbank/sample/'.$key));
 
			$rows[] = $row;

	}
 
	// Individual table headers.
	$header = array();
	$header[] = t('Currency');
	$header[] = t('Merachant ID');
	$header[] = t('Enabled');
	$header[] = t('Action');
 
	$output = theme('table', $header, $rows);

	return $output;

}


function privatbank_currency_edit($form_state, $cur=''){

		$currency_settings = unserialize(variable_get('privatbank_currencies', serialize(_privatbank_GetDefCurSetts())));

		$currency=$currency_settings[$cur];


		// interface data:

		if(!empty($cur)){

			$form['cur'] = array(
			  '#type' => 'item',
			  '#title' => t('Currency'),
			  '#value' => $cur
			);

			$form['currency'] = array(
				'#type' => 'hidden',
				'#value' => $cur,
			);


		}else{

			$form['currency'] = array(
				'#type' => 'textfield', 
				'#title' => t('Currency'), 
				'#description' => t('Enter currency ID string like "USD"'), 
				'#default_value' => '', 
				'#size' => 10, 
				'#maxlength' => 15, 
				'#required' => TRUE
			);

			$currency['account']='';

			$currency['presc']='';

		}

	   

		$form['account'] = array(
			'#type' => 'textfield', 
			'#title' => t('Privatbank merchant ID'), 
			'#default_value' => $currency['account'], 
			'#size' => 20, 
			'#maxlength' => 15, 
			'#required' => FALSE
		);

		$form['presc'] = array(
			'#type' => 'textfield', 
			'#title' => t('Decimals'), 
			'#default_value' => empty($currency['presc']) ? 2 : $currency['presc'], 
			'#size' => 5, 
			'#maxlength' => 3, 
			'#required' => TRUE
		);

		$form['enabled'] = array(
		  '#type' => 'checkbox', 
		  '#title' => t('Enabled'),
		  '#default_value' => $currency['enabled']
		);

		$form['submit'] = array(
			'#type' => 'submit',
			'#value' => t('Save changes'),
		);


		return $form;

}

function privatbank_currency_edit_validate($form, &$form_state){
	

	if (empty($form_state['values']['currency'])) 
		form_set_error('', t('Currency ID does not specified.'));
	
	if($form_state['values']['enabled']=='1'){
		if(empty($form_state['values']['account']))
			form_set_error('account', t('Privatbank merchant ID can not be empty for enabled currency'));
	}


}


function privatbank_currency_edit_submit(&$form, &$form_state){
	
	$currency_settings = unserialize(variable_get('privatbank_currencies', serialize(_privatbank_GetDefCurSetts())));

	$currency_settings["{$form_state['values']['currency']}"]=array(
		'enabled' => $form_state['values']['enabled'],
		'account' => $form_state['values']['account'],
		'presc' =>  (int)$form_state['values']['presc']
	);

	variable_set('privatbank_currencies', serialize($currency_settings));

	drupal_set_message(t("Currency has been saved."));

	$form_state['redirect'] = 'admin/privatbank/currencies';

}



function privatbank_currency_delete($form_state, $cur) {

  $form['cur'] = array(
    '#type' => 'value',
    '#value' => $cur,
  );

  return confirm_form($form,
    t('Are you sure you want to delete %cur ?', array('%cur' => $cur)),
    'admin/privatbank/currencies',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

function privatbank_currency_delete_submit($form, &$form_state) {

	if ($form_state['values']['confirm']){

		$currency_settings = unserialize(variable_get('privatbank_currencies', serialize(_privatbank_GetDefCurSetts())));
		
		$out=array();
		foreach($currency_settings as $key=>$value){
			if($key!=$form_state['values']['cur']) $out[$key]=$value;
		}

		if(count($out)<count($currency_settings)){
			variable_set('privatbank_currencies', serialize($out));
			drupal_set_message(t("Currency has been deleted."));
		}
		else drupal_set_message(t("Currency has NOT been deleted."));


	}else drupal_set_message(t("Currency has NOT been deleted."));

	$form_state['redirect'] = 'admin/privatbank/currencies';

}


// ---------------------------------------------------------------


function privatbank_sample(){

	global $base_url;

  return '<br><h3><u>'.t('Setting up Privatbank Merchant Account').'</u></h3>
<p>'.t('<a href="@privat24-login-url" target="_blank">Login</a> to your Privatbank account and fill in the following fields in Merchant settings:', array('@privat24-login-url'=>'http://privat24.ua/')).'
<table cellpadding="5" width="100%" border=0>
<tbody><tr>
<td nowrap="nowrap">'.t('Web-site URL:').'</td>
<td align="left"><input value="'.$base_url.'" type="text" size=40></td>
</tr>
<tr>
<td nowrap="nowrap">'.t('Service description:').'</td>
<td align="left"><textarea name="" rows="5" cols="40">'.t('Type some description about your site/service in this field.').'</textarea></td>
</tr>
<tr>
<td align="left" colspan=2>'.t('Make sure that the following tick is checked:').'<br>
<input type="checkbox" name="" checked> '.t('Information about payments to merchant').'</td>
</tr>
</tbody></table>

<br>
<h3><u>'.t('Setting up Privatbank module on this site').'</u></h3>
<ol>
<li>'.t('Copy auto-generated <b>Password</b> from Privat24 page and insert it to <a href="@privatbank-settings">Settings page of this module</a>',  array('@privatbank-settings'=>url('admin/privatbank/settings'))).'.
<li>'.t('Copy auto-generated numerical <b>Merchant ID</b> from Privat24 and insert it to one or more <a href="@privatbank-currencies">currencies</a>', array('@privatbank-currencies'=>url('admin/privatbank/currencies'))).'.
</ol>';
}